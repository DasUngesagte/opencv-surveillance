##
#   Based on https://pyimagesearch.com/2015/06/01/home-surveillance-and-motion-detection-with-the-raspberry-pi-python-and-opencv/#download-the-code
##

# import the necessary packages
from datetime import datetime, timedelta
from os import path, listdir, remove
from pathlib import Path
import threading
import argparse
import cv2
import datetime
import imutils
import json
import logging
import time
import warnings

# Periodically removes old images:
from EMail import mailer


class Remover(threading.Thread):

    def __init__(self, save_dir, cutoff_days, logger):
        super().__init__()
        self.save_dir = save_dir
        self.cutoff_days = cutoff_days
        self.logger = logger

    def run(self):
        while True:
            # Check every day:
            time.sleep(60 * 60 * 24)
            try:
                for filename in listdir(self.save_dir):
                    if filename.endswith(".jpg"):
                        cutoff = datetime.utcnow() - timedelta(days=self.cutoff_days)
                        mtime = datetime.utcfromtimestamp(path.getmtime(self.save_dir + filename))
                        if mtime < cutoff:
                            file = "{path}{filename}".format(path=self.save_dir, filename=filename)
                            self.logger.info("Removing {}".format(file))
                            remove(file)
            except Exception as ex:
                self.logger.warning("Error checking or removing files:\n{}".format(ex))


class Surveillance(threading.Thread):

    def __init__(self, conf, logger):
        super().__init__()
        self.stopped = False
        self.logger = logger
        self.load_config(conf)
        self.load_camera()

        self.lastUploaded = datetime.datetime(1, 1, 1, 0, 0)  # Oldest date possible
        self.lastMail = datetime.datetime(1, 1, 1, 0, 0)

    def load_config(self, conf):
        self.cam_id = conf["cam_id"]
        self.color = conf["color"]
        self.resolution = conf["resolution"]
        self.fps = conf["fps"]
        self.show_video = conf["show_video"]
        self.save_images = conf["save_images"]
        self.save_images_dir = conf["save_images_dir"]
        self.cutoff_days = conf["cutoff_days"]
        self.min_upload_seconds = conf["min_upload_seconds"]
        self.min_motion_frames = conf["min_motion_frames"]
        self.delta_thresh = conf["delta_thresh"]
        self.min_area = conf["min_area"]

        self.use_mail = conf["use_mail"]
        self.mail_cooldown_seconds = conf["mail_cooldown_seconds"]
        self.mail_file = conf["mail_file"]

    def load_camera(self):
        # Grab the camera:
        self.cam = cv2.VideoCapture(0)
        if self.cam is None or not self.cam.isOpened():
            cam = cv2.VideoCapture(-1)
        if self.cam is None or not self.cam.isOpened():
            self.logger.warning("Error opening cam!")
            exit(-1)

        if not self.cam.set(cv2.CAP_PROP_FPS, self.fps):
            self.logger.warning("Unsupported fps!")
        if not self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, self.resolution[0]):
            self.logger.warning("Unsupported width!")
        if not self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, self.resolution[1]):
            self.logger.warning("Unsupported height!")
        self.logger.info("Opened camera: {}x{}@{}fps".format(self.cam.get(cv2.CAP_PROP_FRAME_WIDTH),
                                                             self.cam.get(cv2.CAP_PROP_FRAME_HEIGHT),
                                                             self.cam.get(cv2.CAP_PROP_FPS)))

    def run(self):

        # Allow the camera to warm up, then initialize the average frame, last
        # uploaded timestamp, and frame motion counter
        self.logger.info("Warming up...")
        time.sleep(2)
        avg = None
        motionCounter = 0

        # Capture frames from the camera
        while True:

            if self.stopped:
                self.cam.release()
                break

            # Grab the raw NumPy array representing the image and initialize
            # the timestamp and occupied/unoccupied text
            ret_val, frame = self.cam.read()
            timestamp = datetime.datetime.now()
            text = "None"

            # Resize the frame, convert it to grayscale, and blur it
            frame = imutils.resize(frame, width=self.resolution[0])
            if not self.color:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                gray = frame.copy()
            else:
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)

            # If the average frame is None, initialize it
            if avg is None:
                self.logger.info("Starting background model...")
                avg = gray.copy().astype("float")
                continue

            # Accumulate the weighted average between the current frame and
            # previous frames, then compute the difference between the current
            # frame and running average
            cv2.accumulateWeighted(gray, avg, 0.5)
            frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

            # Threshold the delta image, dilate the thresholded image to fill
            # in holes, then find contours on thresholded image
            thresh = cv2.threshold(frameDelta, self.delta_thresh, 255,
                                   cv2.THRESH_BINARY)[1]
            thresh = cv2.dilate(thresh, None, iterations=2)
            cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)
            cnts = imutils.grab_contours(cnts)

            # Loop over the contours
            for c in cnts:

                # If the contour is too small, ignore it
                if cv2.contourArea(c) < self.min_area:
                    continue

                # Compute the bounding box for the contour, draw it on the frame,
                # and update the text
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (128, 255, 128), 2)
                text = "Motion"

            # Draw the text and timestamp on the frame
            ts_label = timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
            text_label = "{} Status: {}".format(self.cam_id, text)

            text_size, _ = cv2.getTextSize(text_label, cv2.FONT_HERSHEY_PLAIN, 1, 1)
            ts_size, _ = cv2.getTextSize(ts_label, cv2.FONT_HERSHEY_PLAIN, 1, 1)
            text_w, text_h = text_size
            ts_w, ts_h = ts_size

            # With margin of 2 px
            cv2.rectangle(frame, (0, 0), (text_w + 4, text_h + 4), (0, 0, 0), -1)
            cv2.rectangle(frame, (0, frame.shape[0] - ts_h - 4), (ts_w + 4, frame.shape[0]), (0, 0, 0), -1)

            cv2.putText(frame, text_label, (2, text_h + 2), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 1)
            cv2.putText(frame, ts_label, (2, frame.shape[0] - 2), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 1)

            # Check to see if the room is occupied
            if text == "Motion":

                # Check to see if enough time has passed between uploads
                if (timestamp - self.lastUploaded).seconds >= self.min_upload_seconds:

                    # Increment the motion counter
                    motionCounter += 1

                    # Check to see if the number of frames with consistent motion is
                    # high enough
                    if motionCounter >= self.min_motion_frames:

                        self.logger.info("Detected motion.")

                        if self.save_images:
                            threading.Thread(target=self.handle_image,
                                             args=(frame.copy(), timestamp), daemon=False).start()

                        # Update the last uploaded timestamp and reset the motion counter
                        self.lastUploaded = timestamp
                        motionCounter = 0

            # Otherwise, the room is not occupied
            else:
                motionCounter = 0

            # Check to see if the frames should be displayed to screen
            if self.show_video:
                # Display the security feed
                cv2.imshow("Security Feed", frame)
                key = cv2.waitKey(1) & 0xFF
                # If the `q` key is pressed, break from the loop
                if key == ord("q"):
                    self.cam.release()
                    break

    def handle_image(self, image, timestamp):
        image_path = self.write_image(image, timestamp)
        if self.use_mail and (timestamp - self.lastMail).seconds >= self.mail_cooldown_seconds:
            subject = "Camera {}: Motion Detected at {}".format(self.cam_id, timestamp)
            message = "Camera {} detected motion at {}.\nLast mail was sent at {}.".format(self.cam_id, timestamp,
                                                                                           self.lastMail)
            mailer.create_and_send_mail(self.logger, self.mail_file, image_path, subject, message)
            self.lastMail = timestamp

    def write_image(self, image, timestamp):
        name = "{path}/{id}/{id}_{time}.jpg".format(path=self.save_images_dir, id=self.cam_id,
                                                    time=timestamp.strftime("%m_%d_%Y-%H_%M_%S"))
        self.logger.info("Writing image to: {}".format(name))
        try:
            cv2.imwrite(name, image)
        except Exception as ex:
            self.logger.warning("Error writing image:\t{}".format(ex))
        return name

    def stop_thread(self):
        self.logger.info("Surveillance-Thread {} is stopping.".format(threading.get_ident()))
        self.stopped = True


class Main():

    def __init__(self):
        logging.basicConfig(
            format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
            level=logging.INFO,
            datefmt='%Y-%m-%d %H:%M:%S')
        self.logger = logging.getLogger()
        self.logger.info("Surveillance running.")

        # Construct the argument parser and parse the arguments
        ap = argparse.ArgumentParser()
        ap.add_argument("-c", "--conf", required=True,
                        help="path to the JSON configuration file")
        args = vars(ap.parse_args())

        self.conf = self.load_config(args)

        # Check for folder to save images to and init remover for each:
        self.remover = None

        if self.conf["save_images"]:
            self.logger.info(
                "Setting up local path: {path}/{id}".format(path=self.conf["save_images_dir"], id=id))
            Path("{path}/{id}".format(path=self.conf["save_images_dir"], id=self.conf["cam_id"])).mkdir(parents=True,
                                                                                                        exist_ok=True)
            self.remover = Remover(self.conf["save_images_dir"], self.conf["cutoff_days"], self.logger)
            self.remover.start()

    def start(self):
        surveillance = Surveillance(self.conf, self.logger)
        surveillance.start()

    def load_config(self, args):
        # Filter warnings, load the configuration
        warnings.filterwarnings("ignore")
        conf = json.load(open(args["conf"]))
        return conf


if __name__ == "__main__":
    main = Main()
    main.start()
