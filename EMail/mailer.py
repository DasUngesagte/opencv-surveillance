import smtplib
import mimetypes
from email.message import EmailMessage


def send_email(sender, password, to, message):
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login(sender, password)
    server.send_message(message, from_addr=sender, to_addrs=to)
    server.close()

def create_message(sender, to, subject, message_text):
    msg = EmailMessage()
    msg.set_content(message_text)
    msg['to'] = to
    msg['from'] = sender
    msg['subject'] = subject
    return msg
    
def create_message_with_attachment(sender, to, subject, message_text, file):
    msg = EmailMessage()
    msg.set_content(message_text)
    msg['to'] = to
    msg['from'] = sender
    msg['subject'] = subject

    content_type, encoding = mimetypes.guess_type(file)

    with open(file, 'rb') as fp:
        maintype, subtype = content_type.split('/', 1)
        msg.add_attachment(fp.read(), maintype=maintype, subtype=subtype, filename=file)
        fp.close()

    return msg

def create_and_send_mail(logger, mail_file, file, subject, message):

    # A simple file with the following three lines and nothing else:
    # sender@mail.com
    # password123
    # receiver@mail.com

    try:
        mail_options = open(mail_file, 'r')
        sender, password, receiver = mail_options.read().splitlines()

        if file is None:
            created_message = create_message(sender, receiver, subject, message)
        else:
            created_message = create_message_with_attachment(sender, receiver, subject, message, file)

        send_email(sender, password, receiver, created_message)
        logger.info("Sending mail to {}.".format(receiver))

    except Exception as ex:
        logger.warning("Error sending mail:\n{}".format(ex))

